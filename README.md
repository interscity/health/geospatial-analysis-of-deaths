# Geospatial analysis of deaths

This is a master's research project at the University of São Paulo that uses advanced data science techniques to analyze geospatial data of deaths in the city of São Paulo.

## Getting started

This system aims to extract, process and consolidate geospatial mortality data for the city of São Paulo in order to support geospatial clustering analyses.

When running the notebooks in their order, the following steps will be performed:
1. Data extraction and consolidation;
2. Data preparation;
3. Application of the machine learning algorithm.

Data extraction is performed separately for three datasets: deaths (TABNET¹), population estimates (TABNET¹) and geospatial data of the administrative districts of the city of São Paulo (SMDU²).

¹Online platform for public access that allows users to query healthcare data.

²Secretaria Municipal de Desenvolvimento Urbano (Municipal Department of Urban Development).

This system contains several notebooks:
- **0. Libraries install**: contains all the library installations needed to run all other notebooks;
- **1. Inputs**: contains some necessary configurations for the functioning of the system will be defined;
- **2. Data extraction**: contains the extraction of all the data mentioned earlier;
- **3. Treatment and consolidation of data**: contains all necessary processing steps and consolidation of all data used;
- **4. Clustering**: contains the final data clusterizations used in this master's project.
For more information, please refer to the "User Manual" file.

## Prerequisites

This project is being developed with Python version 3.8.

## Usage

You can run the notebooks in the order they are arranged in the "Notebooks" folder to obtain the final results according to your desired configurations. Notebooks "2.1. Preliminary Analysis", "2.2. Descriptive Analysis", "3.1. Rate maps" and "4.1. LISA Cluster" provide additional analyses conducted during this master's research, which are not mandatory for running the system itself but are quite insightful for more detailed analysis.

## License

This project is licensed under the MPL 2.0. License.

## Contact

If you any questions, comments, or suggestions about the tool feel free to contact us:
- interscity@ime.usp.br